local mymodule = {}

mymodule.default_action = "status"

mymodule.status = function( self )
	return self.model.get_status()
end

mymodule.startstop = function( self )
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

mymodule.listfiles = function( self )
	return self.model.list_files()
end

mymodule.editfile = function( self )
	return self.handle_form(self, self.model.get_file, self.model.update_file, self.clientdata, "Save", "Edit File", "File Saved")
end

function mymodule.createfile(self)
	return self.handle_form(self, self.model.getnewfile, self.model.createfile, self.clientdata, "Create", "Create New Freeswitch File", "Freeswitch File Created")
end

function mymodule.deletefile(self)
	return self.handle_form(self, self.model.getdeletefile, self.model.deletefile, self.clientdata, "Delete", "Delete Freeswitch File", "Freeswitch File Deleted")
end

function mymodule.reloadxml(self)
	return self.handle_form(self, self.model.getreloadxml, self.model.reload_xml, self.clientdata, "Reload", "Reload Freeswitch XML")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule

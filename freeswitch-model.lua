local mymodule = {}

-- Load libraries
modelfunctions = require("modelfunctions")
posix = require("posix")
fs = require("acf.fs")
format = require("acf.format")
validator = require("acf.validator")
xml = require("LuaXml")

-- Set variables
local processname = "freeswitch"
local packagename = "freeswitch"
local baseurl = "/etc/freeswitch"
local configfile = "/var/log/freeswitch/freeswitch.xml.fsxml"

-- ################################################################################
-- LOCAL FUNCTIONS

local is_valid_filename = function(filename)
	local dirname = posix.dirname(filename)
	return validator.is_valid_filename(filename) and string.match(dirname, baseurl) and not string.match(dirname, "%.%.")
end

-- ################################################################################
-- PUBLIC FUNCTIONS

mymodule.get_status = function()
	return modelfunctions.getstatus(processname, packagename, "Freeswitch Status")
end

function mymodule.get_startstop(self, clientdata)
        return modelfunctions.get_startstop(processname)
end

function mymodule.startstop_service(self, startstop, action)
        return modelfunctions.startstop_service(startstop, action)
end

function mymodule.getreloadxml()
	return cfe({ type="group", value={}, label="Reload Freeswitch XML" })
end

function mymodule.reload_xml(self, relo)
	relo.descr, relo.errtxt = modelfunctions.run_executable({"fs_cli", "-x", "reloadxml"}, true)
	return relo
end

mymodule.get_file = function(self, clientdata)
	local filename = clientdata.filename
	return modelfunctions.getfiledetails(filename, is_valid_filename)
end

mymodule.update_file = function(self, filedetails)
	local ret = modelfunctions.setfiledetails(self, filedetails, is_valid_filename)
	if not ret.errtxt then
		posix.chmod(filedetails.value.filename.value, "rw-------")
		posix.chown(filedetails.value.filename.value, posix.getpasswd("freeswitch", "uid") or 0, posix.getpasswd("freeswitch", "gid") or 0)
	end
	return ret
end

mymodule.list_files = function()
	local retval = {}
	for file in fs.find(null, baseurl) do
		local details = posix.stat(file)
		if details.type == "regular" then
			details.filename = file
			table.insert(retval, details)
		end
	end
	table.sort(retval, function(a,b) return a.filename < b.filename end)
	return cfe({ type="structure", value=retval, label="List of Freeswitch files" })
end

function mymodule.getnewfile()
	local filename = cfe({ label="File Name", descr="Must be in "..baseurl })
	return cfe({ type="group", value={filename=filename}, label="Freeswitch File" })
end

function mymodule.createfile(self, filedetails)
	local success = true
	local path = string.match(filedetails.value.filename.value, "^%s*(.*%S)%s*$") or ""
	if not string.find(path, "/") then
		path = baseurl.."/"..path
	end

	if not is_valid_filename(path) then
		success = false
		filedetails.value.filename.errtxt = "Invalid filename"
	else
		if not fs.is_dir(baseurl) then fs.create_directory(baseurl) end
		if posix.stat(path) then
			success = false
			filedetails.value.filename.errtxt = "Filename already exists"
		end
	end

	if success then
		fs.create_file(path)
	else
		filedetails.errtxt = "Failed to Create File"
	end

	return filedetails
end

function mymodule.getdeletefile(self, clientdata)
	local retval = {}
	retval.filename = cfe({ label="File Name", value=clientdata.filename or "" })
	return cfe({ type="group", value=retval, label="Delete Freeswitch File" })
end

function mymodule.deletefile(self, delfile)
	delfile.errtxt = "Failed to delete Freeswitch File - invalid filename"
	for i,file in ipairs(mymodule.list_files().value) do
		if delfile.value.filename.value == file.filename then
			delfile.errtxt = nil
			os.remove(delfile.value.filename.value)
			break
		end
	end

	return delfile
end

function mymodule.get_logfile(self, clientdata)
	local retval = cfe({ type="structure", value={}, label="Log File Configuration" })
	local config
	local res, err = pcall(function()
		config = xml.load(configfile)
	end)
	if not res and err then
		retval.errtxt = err
		config = xml.new()
	end

	if (config:find("load", "module", "mod_logfile")) then
		local logfileparam
		local logfileconfig = config:find("configuration", "name", "logfile.conf")
		-- There may be multiple profiles, but for now just use the first one found
		if logfileconfig then
			logfileparam = logfileconfig:find("param", "name", "logfile")
		end
		if logfileparam then
			retval.value[#retval.value+1] = {filename=logfileparam.value}
		else
			retval.value[#retval.value+1] = {filename="/var/log/freeswitch/freeswitch.log"}
		end
	end
	if (config:find("load", "module", "mod_syslog")) then
		local syslog = config:find("configuration", "name", "syslog.conf")
		local facility = syslog:find("param", "name", "facility") or {value="user"}
		local ident = syslog:find("param", "name", "ident") or {value="freeswitch"}
		retval.value[#retval.value+1] = {facility=facility.value, grep=ident.value}
	end

	return retval
end

return mymodule
